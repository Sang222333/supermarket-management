# supermarket management 

|Title| Manager Add product information|
|---|---|
|Value statement|As a manager , I want add product information|
|Acceptance Criteria| Acceptance Criterion :Given The product has not been added to the system  When we will enter product information into the system, the system will add product information to the database|
|Definition of Done| Unit Tests Passed <br/> Acceptance Criteria Met <br/>Code Reviewed <br/>Functional Tests Passed <br/>Non-Functional Requiremets Met <br/>Product owner accept user story|
|Owner| Nguyen Tan Sang   |


|Title| Manager Add list of products|
|---|---|
|Value statement|As a manager , I want add list of products |
|Acceptance Criteria| Acceptance Criterion :Given List of products has not been added to the system When we want to enter list of products into the system, the system will add list of products to the database|
|Definition of Done| Unit Tests Passed <br/> Acceptance Criteria Met <br/>Code Reviewed <br/>Functional Tests Passed <br/>Non-Functional Requiremets Met <br/>Product owner accept user story|
|Owner| Nguyen Tan Sang   |


|Title| Manager update invoice|
|---|---|
|Value statement|As a manager , I want update invoice |
|Acceptance Criteria| Acceptance Criterion :Given The invoice has not been updated to the system When we want to update invoice into the system, the system will update invoice to the database|
|Definition of Done| Unit Tests Passed <br/> Acceptance Criteria Met <br/>Code Reviewed <br/>Functional Tests Passed <br/>Non-Functional Requiremets Met <br/>Product owner accept user story|
|Owner| Nguyen Tan Sang   |
